#! /bin/bash
grep -rl 'https://dev.' nuxt.config.js | xargs sed -i 's/https:\/\/dev./https:\/\//g'
#grep -rl 'https://p.' nuxt.config.js | xargs sed -i 's/https:\/\/p./https:\/\//g'
grep -rl 'DEVELOP' nuxt.config.js | xargs sed -i 's/DEVELOP/PRODUCTION/g'
#grep -rl '_blank")' components/utilities/comprarLinea.vue | xargs sed -i 's/_blank\")/_self\")/g'
