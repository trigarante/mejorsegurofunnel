module.exports = {
    target: "static",

    /*
     ** Headers of the page
     */
    head: {
        title: "Mejor Seguro de Auto",
        htmlAttrs: {
            lang: "es",
        },
        meta: [
            { charset: "utf-8" },
            { name: "viewport", content: "width=device-width, initial-scale=1" },
            {
                hid: "description",
                name: "description",
                content: "Mejor Seguro de Auto",
            },
        ],
        link: [{ rel: "icon", type: "image/png", href: "/favicon.png" }],
    },
    /*
     ** Customize the progress bar color
     */
    loading: { color: "#3bd600" },

    css: ["static/css/bootstrap.min.css", "static/css/styles.css"],
    /*
     ** Build configuration
     */
    build: {
        //analyze:true,

        /*
         ** Run ESLint on save
         */
        extend(config, { isDev, isClient }) {
            if (isDev && isClient) {
                config.module.rules.push({
                    enforce: "pre",
                    test: /\.(js|vue)$/,
                    //loader: "eslint-loader",
                    exclude: /(node_modules)/,
                });
            }
        },
    },
    // include bootstrap css

    plugins: [
        { src: "~/plugins/filters.js", ssr: false },
        { src: "~/plugins/apm-rum.js", ssr: false },
    ],

    modules: ["@nuxtjs/axios"],
    env: {
        /*PRUEBAS*/
        newCoreMejorSeguro: "https://dev.core-mejorseguro.com", // CORE
        catalogo: "https://dev.ws-qualitas.com",

        urlValidaciones: "https://core-blacklist-service.com/rest", // VALIDACIONES
        urlWSAutos: "https://ahorraseguros.mx/ws-autos/servicios",
        urlDB: "https://ahorraseguros.mx/ws-rest/servicios",
        urlNewDataBase: "https:/ahorraseguros.mx/servicios",
        promoCore: "https://dev.core-persistance-service.com",
        Environment: "DEVELOP",
    },
    render: {
        http2: { push: true },
        resourceHints: false,
        compressor: { threshold: 9 },
    },
};