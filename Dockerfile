FROM node:16.13.1-alpine

ENV NODE_ENV=production

RUN mkdir /app
WORKDIR /app
COPY ["package.json", "package-lock.json*", "./"]
ARG DOCKER_HOST=00.00.00.00
RUN rm -Rf .nuxt
RUN rm -rf node_modules package-lock.json dist

# Install app dependencies
RUN npm install
RUN npm run build

COPY . .

RUN apk add curl

ADD ./script/updateMarcas.sh ./script/updateMarcas.sh

RUN chmod +x ./script/updateMarcas.sh

RUN ["sh","./script/updateMarcas.sh"]

RUN sed -i "s/127.0.0.1/${DOCKER_HOST}/g" package.json

EXPOSE 3000

RUN npm run generate

# set the startup command to execute
CMD  ["npm", "start"]