import axios from "axios";

const interaccionService = {};

interaccionService.newInteraccion = function (peticion, accessToken) {
  return axios({
    method: "post",
    headers: { Authorization: `Bearer ${accessToken}` },
    url: process.env.promoCore + "/v3/interaction/request_online",
    data: JSON.parse(peticion),
  });
};
export default interaccionService;
