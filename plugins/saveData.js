import axios from 'axios'
import configDB from './configBase'

const cotizacionService = {}

cotizacionService.search = function (peticion,accessToken) {

    return axios({
        method: "post",
        headers: { Authorization: `Bearer ${accessToken}` },
        //GENERAL SALESFORCE
        url: process.env.promoCore + '/v3/general/car',
        data: JSON.parse(peticion)
    })
}
export default cotizacionService
