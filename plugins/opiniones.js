import opiBase from './opiBase'

const opinionesService = {}

opinionesService.search = function () {
  return opiBase.get('/api.php',{
    headers: { 'Access-Control-Allow-Origin': '*' },
  })
    .then(res => res.data)
    .catch(err => console.error(err));

}
export default opinionesService
