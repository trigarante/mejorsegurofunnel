const endpoints = {
  'ABA':
  {
    'marcas': 968,
    'modelos': 972,
    'submarcas': 971,
    'descripciones': 973,
  },
  'AFIRME':
  {
    'marcas': 974,
    'modelos': 978,
    'submarcas': 977,
    'descripciones': 979,
  },
  'AIG':
  {
    'marcas': 980,
    'modelos': 984,
    'submarcas': 983,
    'descripciones': 985,
  },
  'ANA':
  {
    'marcas': 986,
    'modelos': 988,
    'submarcas': 987,
    'descripciones': 989,
  },
  'ATLAS':
  {
    'marcas': 991,
    'modelos': 993,
    'submarcas': 992,
    'descripciones': 994,
  },
  'AXA':
  {
    'marcas': 995,
    'modelos': 999,
    'submarcas': 998,
    'descripciones': 1000,
  },
  'BANORTE':
  {
    'marcas': 1001,
    'modelos': 1004,
    'submarcas': 1003,
    'descripciones': 1005,
  },
  'CABIFY':
  {
    'marcas': 54,
    'modelos': 64,
    'descripciones': 65,
    'subdescripciones': 66,
    'detalles': 68,
  },
  'DIDI':
  {
    'marcas': 54,
    'modelos': 64,
    'descripciones': 65,
    'subdescripciones': 66,
    'detalles': 68,
  },
  'ELAGUILA':
  {
    'marcas': 1012,
    'modelos': 1016,
    'submarcas': 1015,
    'descripciones': 1017,
  },
  'ELPOTOSI':
  {
    'marcas': 1018,
    'modelos': 1020,
    'submarcas': 1019,
    'descripciones': 1021,
  },
  'GS':
  {
    'marcas': 1022,
    'modelos': 1026,
    'submarcas': 1025,
    'descripciones': 1027,
  },
  'GLOBAL':
  {
    'marcas': 54,
    'modelos': 64,
    'descripciones': 65,
    'subdescripciones': 66,
    'detalles': 68,
  },
  'GNP':
  {
    'marcas': 1028,
    'modelos': 1030,
    'submarcas': 1029,
    'descripciones': 1031,
  },
  'HDI':
  {
    'marcas': 1032,
    'modelos': 1036,
    'submarcas': 1035,
    'descripciones': 1037,
  },
  'INBURSA':
  {
    'marcas': 54,
    'modelos': 64,
    'descripciones': 65,
    'subdescripciones': 66,
    'detalles': 68,
  },
  'LATINO':
  {
    'marcas': 55,
    'modelos': 57,
    'submarcas': 56,
    'descripciones': 58,
    'detalle': 59
  },
  'MAPFRE':
  {
    'marcas': 1038,
    'modelos': 1042,
    'submarcas': 1041,
    'descripciones': 1043,
  },
  'MIITUO':
  {
    'marcas': 54,
    'modelos': 64,
    'descripciones': 65,
    'subdescripciones': 66,
    'detalles': 68,
  },
  'MULTIVA':
  {
    'marcas': 54,
    'modelos': 64,
    'descripciones': 65,
    'subdescripciones': 66,
    'detalles': 68,
  },
  'QUALITAS':
  {
    'marcas': 455,
    'modelos': 452,
    'submarcas': 451,
    'descripciones': 453,
    'subdescripciones': 454
  },
  'SURA':
  {
    'marcas': 1051,
    'modelos': 1055,
    'submarcas': 1054,
    'descripciones': 1056,
  },
  'UBER':
  {
    'marcas': 54,
    'modelos': 64,
    'descripciones': 65,
    'subdescripciones': 66,
    'detalles': 68,
  },
  'BXMAS':
  {
    'marcas': 1057,
    'modelos': 1061,
    'submarcas': 1060,
    'descripciones': 1062,
  },
  'ZURICH':
  {
    'marcas': 1063,
    'modelos': 1067,
    'submarcas': 1066,
    'descripciones': 1068,
  }
}

export default endpoints;