import axios from 'axios'
import configDB from './configBase'

const saveSolicitudService = {}

saveSolicitudService.nuevoContacto = function (peticion, accessToken) {

    return axios({
        method: "post",
        headers: { Authorization: `Bearer ${accessToken}` },
        url: configDB.baseUrl + '/solicitudes',
        data: JSON.parse(peticion)
    })
}
export default saveSolicitudService