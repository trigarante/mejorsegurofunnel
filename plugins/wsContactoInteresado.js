import axios from 'axios'


const wsContactoInteresado = {}

wsContactoInteresado.nuevoContacto = function (peticion) {

  return axios({
    method: "post",
    url: process.env.urlNewDataBase + '/interesadoVL',
    data: JSON.parse(peticion)
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... no se pudo guardar el nuevo Contacto:( "+err));
}
export default wsContactoInteresado
