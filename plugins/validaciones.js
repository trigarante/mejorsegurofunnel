import * as axios from "axios";

class validacionesService {
  validarCorreo(email) {
    return axios({
      method: "get",
      url: process.env.urlValidaciones + `/validacion/correo?email=` + email,
    });
  }

  validarTelefono(telefono) {
    return axios({
      method: "get",
      url:
        process.env.urlValidaciones +
        "/validacionTel/validacion?telefono=" +
        telefono,
    });
  }

  validarCodigoPostal(cp, accessToken) {
    return axios({
      method: "get",
      headers: {
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${accessToken}`,
      },
      //CP  salesForce
      url: process.env.promoCore + "/v2/sepomex/" + cp,
    });
  }
  validarEstado(aseguradora, estado, accessToken) {
    aseguradora = aseguradora.toLowerCase();
    return axios({
      method: "get",
      headers: {
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${accessToken}`,
      },
      url:
        process.env.newCoreMejorSeguro +
        `/v1/${aseguradora}/descuentos?estado=` +
        estado,
    });
  }
}
export default validacionesService;
