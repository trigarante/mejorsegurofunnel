import axios from 'axios'

const descuentoService ={}

descuentoService.search=function (estado, accessToken) {
	
	return axios({
		method: "get",
		headers: {
			'Access-Control-Allow-Origin': '*',
			Authorization: `Bearer ${accessToken}`
		},
		url: process.env.newCoreMejorSeguro+ `/v1/afirme/descuentos?estado=`+ estado

	})	
}
export default descuentoService
