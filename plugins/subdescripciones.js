import autosService from './ws-autos'

const subdescripcionesService ={}

subdescripcionesService.search=function (aseguradora, marca, modelo, descripcion, accessToken) {
  return autosService.get('/subdescripciones',{
    headers: { Authorization: `Bearer ${accessToken}` },
    params:{aseguradora,marca,modelo,descripcion}
  })
}
export default subdescripcionesService
