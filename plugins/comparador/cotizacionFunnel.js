import axios from 'axios'

const cotizacionInternaService = {}

cotizacionInternaService.search = function (data, accessToken) {
    return axios({
      method: "get",
      headers: { Authorization: `Bearer ${accessToken}` },
      url: process.env.newCoreMejorSeguro+  '/v1/extras/cotizar',
      params: {request:data}
    })
  }

export default cotizacionInternaService
