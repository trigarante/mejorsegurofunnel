import autosService from '../ws-autos'

const subdescripcionesService ={}

subdescripcionesService.search=function (marca, modelo, submarca, accessToken) {
  return autosService.get('comparador/descripciones',{
    headers: { Authorization: `Bearer ${accessToken}` },
    params:{marca,modelo,submarca}
  })
}
export default subdescripcionesService
