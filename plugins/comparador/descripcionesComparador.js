import autosService from '../ws-autos'

const descripcionesService ={}

descripcionesService.search=function (marca, modelo, accessToken) {
  return autosService.get('comparador/submarcas',{
    headers: { Authorization: `Bearer ${accessToken}` },
    params:{marca,modelo}
  })
}
export default descripcionesService
