import axios from 'axios'


const wsLeadVendido = {}

wsLeadVendido.nuevoLeadVendido = function (peticion, accessToken) {

    return axios({
        method: "post",
        headers: { Authorization: `Bearer ${accessToken}` },
        url: process.env.newCoreMejorSeguro + '/v1/leads',
        data: JSON.parse(peticion)
    })
}
export default wsLeadVendido