import autosService from "../plugins/ws-autos";

let catalogos = {
  catalogo: process.env.catalogo + "/v3/qualitas-car",
};

class CatalogosDirectos {
  marcas() {
    return autosService.get(catalogos.catalogo + `/brands`);
  }

  modelos(marca) {
    return autosService.get(catalogos.catalogo + `/years?brand=${marca}`);
  }

  submarcas(marca, modelo) {
    return autosService.get(
      catalogos.catalogo + `/models?brand=${marca}&year=${modelo}`
    );
  }

  descripciones(marca, modelo, submarca) {
    return autosService.get(
      catalogos.catalogo +
        `/variants?brand=${marca}&year=${modelo}&model=${submarca}`
    );
  }
}

export default CatalogosDirectos;
