import autosService from './ws-autos'

const descripcionesService ={}

descripcionesService.search=function (aseguradora, marca, modelo, accessToken) {
  return autosService.get('/descripciones',{
    headers: { Authorization: `Bearer ${accessToken}` },
    params:{aseguradora,marca,modelo}
  })
}
export default descripcionesService
