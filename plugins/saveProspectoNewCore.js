import axios from 'axios'


const newProspectNewCoreService = {}

newProspectNewCoreService.nuevoProspecto = function (peticion, accessToken)
{
    return axios({
        method: "post",
        headers: {'Authorization': 'Bearer '+accessToken},
        url: process.env.newCoreMejorSeguro + '/v2/cotizaciones/comparadores',
        data: JSON.parse(peticion)
    })
}
export default newProspectNewCoreService


