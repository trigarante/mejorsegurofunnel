import axios from "axios";

const cotizacionAliService = {};

cotizacionAliService.nuevaCotizacion = function (
  peticion,
  accessToken,
  cotizacionAli
) {
  return axios({
    method: "put",
    headers: { Authorization: "Bearer " + accessToken },
    url: process.env.promoCore + `/v1/cotizaciones-ali/${cotizacionAli}`,
    data: JSON.parse(peticion),
  });
};
export default cotizacionAliService;
