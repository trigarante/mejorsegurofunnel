import axios from "axios";

const cotizacionInternaService = {};

cotizacionInternaService.search = function (aseguradora, data, accessToken) {
  aseguradora = aseguradora.toLowerCase();
  return axios({
    method: "post",
    headers: { Authorization: "Bearer " + accessToken },
    // url: 'https://core-persistance-service.com/v2/'+`${aseguradora}`+'/quotation',
    url: process.env.promoCore + `/v2/${aseguradora}/quotation`,
    data: JSON.parse(data),
  });
};
export default cotizacionInternaService;
