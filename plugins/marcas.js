import autosService from './ws-autos'

const marcasService ={}

marcasService.search=function (aseguradora, accessToken) {
  return autosService.get('/marcas',{
    headers: {'Access-Control-Allow-Origin': '*',
              Authorization: `Bearer ${accessToken}`},
    params:{aseguradora}
  })
}
export default marcasService
