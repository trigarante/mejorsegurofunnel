import axios from "axios";

const newProspectNewCoreService = {};

newProspectNewCoreService.nuevoProspecto = function(peticion, accessToken) {
  return axios({
    method: "post",
    headers: { Authorization: "Bearer " + accessToken },
    url: process.env.newCoreMejorSeguro + "/v1/cotizaciones",
    data: JSON.parse(peticion),
  });
};
export default newProspectNewCoreService;
