import * as axios from "axios";
import matlas from "~/script/atlas.json";
import maxa from "~/script/axa.json";
import mbxmas from "~/script/vepormas.json";
import melaguila from "~/script/elaguila.json";
import melpotosi from "~/script/el_potosi.json";
import mgs from "~/script/general_seguros.json";
import msura from "~/script/sura.json";
import mzurich from "~/script/zurich.json";
import mmigo from "~/script/migo.json";
class catalogosInternos {
  marca(aseguradora) {
    switch (aseguradora) {
      case "atlas":
        return matlas;
      case "axa":
        return maxa;
      case "elaguila":
        return melaguila;
      case "elpotosi":
        return melpotosi;
      case "generaldeseguros":
        return mgs;
      case "migo":
        return melpotosi;
      case "miituo":
        return mhdi;
      case "multiva":
        return mhdi;
      case "primeroseguros":
      case "sura":
        return msura;
      case "vepormas":
        return mbxmas;
      case "zurich":
        return mzurich;
      case "migo":
        return mmigo;
      default:
        return mqualitas;
    }
  }

  modelo(aseguradora, marca, accessToken) {
    switch (aseguradora) {
      case "ELPOTOSI":
        aseguradora = "EL_POTOSI";
        break;
      case "GS":
        aseguradora = "GENERAL_SEGUROS";
        break;
      case "PRIMEROSEGUROS":
        aseguradora = "PRIMERO_SEGUROS";
        break;
      case "BXMAS":
        aseguradora = "BXMAS";
        break;

      default:
        aseguradora = aseguradora;
    }

      var url = `/v1/${aseguradora.toLowerCase()}/modelos?marca=` + marca;
      return axios({
        method: "get",
        headers: { Authorization: `Bearer ${accessToken}` },
        url: process.env.newCoreMejorSeguro + url,
      });
    
  }

  submarca(aseguradora, marca, modelo, accessToken) {
    switch (aseguradora) {
      case "ELPOTOSI":
        aseguradora = "EL_POTOSI";
        break;
      case "GS":
        aseguradora = "GENERAL_SEGUROS";
        break;
      case "PRIMEROSEGUROS":
        aseguradora = "PRIMERO_SEGUROS";
        break;
      case "BXMAS":
        aseguradora = "BXMAS";
        break;

      default:
        aseguradora = aseguradora;
    }

      var url =
        `/v1/${aseguradora.toLowerCase()}/submarcas?marca=` +
        marca +
        "&modelo=" +
        modelo;
      return axios({
        method: "get",
        headers: { Authorization: `Bearer ${accessToken}` },
        url: process.env.newCoreMejorSeguro + url,
      });
    

  }

  descripcion(aseguradora, marca, modelo, submarca, accessToken) {
    switch (aseguradora) {
      case "ELPOTOSI":
        aseguradora = "EL_POTOSI";
        break;
      case "GS":
        aseguradora = "GENERAL_SEGUROS";
        break;
      case "PRIMEROSEGUROS":
        aseguradora = "PRIMERO_SEGUROS";
        break;
      case "BXMAS":
        aseguradora = "BXMAS";
        break;

      default:
        aseguradora = aseguradora;
    }

      aseguradora = aseguradora.toLowerCase();
      return axios({
        method: "get",
        headers: { Authorization: `Bearer ${accessToken}` },
        url:
          process.env.newCoreMejorSeguro +
          `/v1/${aseguradora}/descripciones?marca=` +
          marca +
          "&modelo=" +
          modelo +
          "&submarca=" +
          submarca,
      });
    
  }
}
export default catalogosInternos;
