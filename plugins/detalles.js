import autosService from './ws-autos'

const detallesService ={}

detallesService.search=function (aseguradora, marca, modelo, descripcion,subdescripcion, accessToken) {
  return autosService.get('/detalles_aseguradora',{
    headers: { Authorization: `Bearer ${accessToken}` },
    params:{aseguradora,marca,modelo,descripcion,subdescripcion}
  })
}
export default detallesService
