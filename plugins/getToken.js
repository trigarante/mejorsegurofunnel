import axios from "axios";

const getTokenService = {};

getTokenService.search = function (tokenData) {
  return axios({
    method: "post",
    // AUTENTICATE SALESFORCE
    url: process.env.promoCore + "/v1/authenticate",
    data: { tokenData },
  });
};
export default getTokenService;
