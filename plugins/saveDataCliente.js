import axios from "axios";

const saveDataCliente = {};

saveDataCliente.search = function(peticion, accessToken) {
    return axios({
        method: "post",
        headers: { Authorization: `Bearer ${accessToken}` },
        //LogDatos SALESfORCE
        url: process.env.promoCore + "/v2/json_quotation/customer_record",
        data: JSON.parse(peticion),
    });
};



export default saveDataCliente;