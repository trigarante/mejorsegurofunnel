import axios from 'axios'


const newLeadEmision = {}

newLeadEmision.newLeadEm = function (peticion) {

  return axios({
    method: "post",
    url: process.env.urlNewEmision + '/emision',
    data: JSON.parse(peticion)
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... no se pudo guardar la nueva cotizacion en emision:( "+err));
}
export default newLeadEmision
