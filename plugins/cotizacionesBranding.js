import axios from "axios";

const cotizacionBranding = {};

cotizacionBranding.newCotizacion = function (peticion, accessToken) {
  return axios({
    method: "post",
    headers: { Authorization: "Bearer " + accessToken },
    url: process.env.promoCore + "/v3/cotizaciones/branding",
    data: JSON.parse(peticion),
  });
};
export default cotizacionBranding;
