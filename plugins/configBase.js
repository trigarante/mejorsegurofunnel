
const dbService = {
  baseUrl: 'https://mejorsegurodeauto.mx/ws-rest/servicios',
  baseAutos: 'https://mejorsegurodeauto.mx/ws-autos/servicios',
  urlNewDataBase: 'https://ahorraseguros.mx/servicios',
  tokenData: "MxQkZ8IhziDRxAl1PG4zvhQkYsx5XKa/mHxuu2nOXfM=",
  baseUrlViejo: 'https://ahorraseguros.mx/ws-rest/servicios',
}

export default dbService
