#! /bin/bash
pwd
#Se comprueba que exista el directorio backup dentro de la carpeta script
#En caso de que no exista, se va a crear
if [ -d script/backup ];
then
echo "Sí, sí existe backup."
else
pwd
echo "No, no existe backup, se creará."
cd script || exit
mkdir backup
ls -la
cd ..
fi
#Se indica la aseguradora a la cual se desea consultar las marcass

for var in atlas axa el_potosi sura vepormas zurich elaguila migo general_seguros qualitas hdi; do
  if [ -f script/$var.json ];
  then
    echo "Sí, sí existe "$var", movere archivos"
    mv script/$var.json script/backup/
  else
    echo "No existe "$var" no movere archivos"
  fi

  pet="$(curl https://dev.web-gnp.mx/v3/gnp-car/brand)"
  if [[ $pet =~ ^\[ ]]; then
   echo "Es un array de marcas"
   if [[ ! $pet =~ ^\[\] ]]; then
     echo "Hay marcas en el array, se guardarán en el json"
     echo $pet >> script/$var.json
   else
     echo "El Array está vacio, se traerán las marcas del backup"
     cp script/backup/$var.json script/$var.json
   fi
  else
   echo "No, no es un array, se traerán las marcas del backup"
   cp script/backup/$var.json script/$var.json
  fi
done

#proceso para comparador
if [ -f script/comparadores.json ];
then
  echo "Sí, sí existe comparador, movere archivos"
  mv script/comparadores.json script/backup/
else
  echo "No, no existe comparador, no movere archivos"
fi

pet="$(curl https://dev.core-mejorseguro.com/v1/comparador/marcas)"
if [[ $pet =~ ^\[ ]]; then
 echo "Es un array de marcas"
 if [[ ! $pet =~ ^\[\] ]]; then
   echo "Hay marcas en el array, se guardarán en el json"
   echo $pet >> script/comparadores.json
 else
   echo "El Array está vacio, se traerán las marcas del backup"
   cp script/backup/comparadores.json script/comparadores.json
 fi
else
 echo "No, no es un array, se traerán las marcas del backup"
 cp script/backup/comparadores.json script/comparadores.json
fi