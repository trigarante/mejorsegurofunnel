import Vuex from "vuex";
import getTokenService from "~/plugins/getToken";
import dbService from "../plugins/configBase";
import telApi from "~/plugins/telefonoService.js";

const createStore = () => {
    return new Vuex.Store({
        state: {
            ekomi: false,
            cotizacion: "",
            ambientePruebas: false,
            cargandocotizacion: false,
            msj: false,
            msjEje: false,
            idValid: true,
            idValid2: true,
            cotizacionesAmplia: [],
            cotizacionesLimitada: [],
            sin_token: false,
            tiempo_minimo: 1.3,
            mostrarBoton: false,
            cotizacionInterna: false,
            limpiaCoberturas: "-",
            sinTresCaminos: false,
            isGlobal: false,
            isMiituo: false,
            isMultiva: false,
            isMigo: false,
            isChubb: false,
            control: {
                vista: true,
                modalAseguradora: "0",
            },
            config: {
                time: 5000,
                btnEmisionDisabled: false,
                reload: true,
                especial: false,
                corePer: false,
                aseguradora: "",
                cotizacion: true,
                emision: false,
                descuento: "",
                telefonoAS: "",
                grupoCallback: "",
                from: "",
                loading: false,
                urlRedireccion: "",
                numCotizacionUsuario: "",
                cotizacionAli: "",
                updateAli: false,
                desc: "",
                msi: "",
                promoLabel: "",
                promoSpecial: false,
                promoImg: "",
                funnelDto: "",
                funnelMsi: "",
                extraMsg: "",
                ipCliente: "",
                accessToken: "",
                textoSMS: "Ya tenemos tu cotizacion, en breve un ejecutivo te contactará.",
                img: "aba",
            },

            allAseguradorasAhorraLead: {},
            ejecutivo: {
                nombre: "",
                correo: "",
                id: 0,
            },

            formData: {
                urlOrigen: "",
                aseguradora: "",
                marca: "",
                modelo: "",
                descripcion: "",
                detalle: "",
                clave: "",
                cp: "",
                nombre: "",
                telefono: "",
                gclid_field: "",
                correo: "",
                edad: "",
                fechaNacimiento: "",
                genero: "",
                emailValid: "",
                telefonoValid: "",
                codigoPostalValid: "",
                idLogData: "",
            },
            dataCotizacion: {},
            cotizaciones: [],
            aseguradora_lead: "",
            indice_lead: "",
        },
        actions: {
            getToken() {
                return new Promise((resolve, reject) => {
                    getTokenService.search(dbService.tokenData).then(
                        (resp) => {
                            this.state.config.accessToken = resp.data.accessToken;
                            localStorage.setItem("authToken", this.state.config.accessToken);
                            resolve(resp);
                        },
                        (error) => {
                            reject(error);
                        }
                    );
                });
            },
        },
        mutations: {
            telApi: function(state) {
                try {
                    telApi
                        .search(state.config.idMedioDifusion)
                        .then((resp) => {
                            resp = resp.data;
                            let telefono = resp.telefono;
                            if (parseInt(telefono)) {
                                var tel = telefono + "";
                                var tel2 = tel.substring(2, tel.length);
                                state.config.telefonoAS = tel2;
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                        });
                } catch (error) {
                    console.log(error);
                }
            },

            validarTokenCore: function(state) {
                try {
                    if (process.browser) {
                        if (localStorage.getItem("authToken") === null) {
                            state.sin_token = true;
                            //console.log("NO HAY TOKEN...");
                        } else {
                            state.config.accessToken = localStorage.getItem("authToken");
                            var tokenSplit = state.config.accessToken.split(".");
                            var decodeBytes = atob(tokenSplit[1]);
                            var parsead = JSON.parse(decodeBytes);
                            var fechaUnix = parsead.exp;
                            /*
                             * Fecha formateada de unix a fecha normal
                             * */
                            var expiracion = new Date(fechaUnix * 1000);
                            var hoy = new Date(); //fecha actual
                            /*
                             * Se obtiene el tiempo transcurrido en milisegundos
                             * */
                            var tiempoTranscurrido = expiracion - hoy;
                            /*
                             * Se obtienen las horas de diferencia a partir de la conversión de los
                             * milisegundos a horas.
                             * */
                            var horasDiferencia =
                                Math.round(
                                    (tiempoTranscurrido / 3600000 + Number.EPSILON) * 100
                                ) / 100;

                            if (hoy > expiracion || horasDiferencia < state.tiempo_minimo) {
                                state.sin_token = "expirado";
                            }
                        }
                    }
                } catch (error2) {
                    // console.log(error2)
                }
            },
        },
    });
};

export default createStore;